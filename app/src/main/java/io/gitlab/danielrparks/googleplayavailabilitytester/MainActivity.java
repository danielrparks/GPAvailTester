package io.gitlab.danielrparks.googleplayavailabilitytester;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class MainActivity extends AppCompatActivity {

    private static final String[] ps = new String[]{"com.android.vending", "com.google.android.gms"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.checkButton).setOnClickListener(mainBtnListener);
    }

    View.OnClickListener mainBtnListener = new View.OnClickListener() {
        @SuppressLint("PackageManagerGetSignatures")
        @Override
        public void onClick(View view) {
            int result = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getApplicationContext());
            StringBuilder sResult = new StringBuilder(new ConnectionResult(result).toString());
            sResult.append('\n');
            PackageManager pm = getApplicationContext().getPackageManager();
            PackageInfo pi;
            for (String pn: ps) {
                pi = null;
                try {
                    pi = pm.getPackageInfo(pn, PackageManager.GET_UNINSTALLED_PACKAGES | PackageManager.GET_SIGNATURES);
                } catch (PackageManager.NameNotFoundException e) {
                    Log.e("GPAvailTester", "Missing package: " + pn, e);
                }
                sResult.append(String.format("%s: %s\n", pn, pi != null ? pi.signatures[0].toCharsString().substring(0, 16) : "NameNotFound"));
            }
            ((TextView)findViewById(R.id.resultBox)).setText(sResult.toString());
        }
    };
}